# System-wide .bashrc file for interactive bash(1) shells.

# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
# but only if not SUDOing and have SUDO_PS1 set; then assume smart user.
if ! [ -n "${SUDO_USER}" -a -n "${SUDO_PS1}" ]; then
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

# enable bash completion in interactive shells
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
	function command_not_found_handle {
	        # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
		   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
		   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
		else
		   printf "%s: command not found\n" "$1" >&2
		   return 127
		fi
	}
fi


#############################
####     VARIABLES       ####
#############################

export HISTFILESIZE='1000000'
export HISTTIMEFORMAT=' | %Y%m%d_%H:%M:%S | '
shopt -s histappend

#############################
####     FUNCIONES       ####
#############################

function calc {
	awk "BEGIN {print \"The answer is: \" $* }";
} # end of calc

# mkdir newdir then cd into it
# usage: mcd (<mode>) <dir>
function mcd {
	local newdir='_mcd_command_failed_'
	if [ -d "$1" ]; then
		# Dir exists, mention that...
		echo $1 exists...
	else
		if [ -n "$2" ]; then
			# We've specified a mode
			command mkdir -p -m $1 "$2" && newdir="$2"
		else
			# Plain old mkdir
			command mkdir -p "$1" && newdir="$1"
		fi
	fi
	builtin cd "$newdir"
	# No matter what, cd into it
} # end of mcd


##############################
####       ALIAS          ####
##############################


alias ls='ls --color="auto"'
alias sl='sl -alFe'
alias LS='sl -alFe'
alias fuck='sudo $(history -p \!\!)'
#alias grep='grep --color="auto -n"'

fnd () {
  find -iname "*$1*"
}

sha1b64 () {
	echo -n $1 | openssl dgst -sha1 -binary | openssl enc -base64
}
sha256b64 () {
	echo -n $1 | openssl dgst -sha256 -binary | openssl enc -base64
}
md5b64 () {
	echo -n $1 | openssl dgst -md5 -binary | openssl enc -base64
}


ECBart (){
	if [[ $1 == *'.jpg' ]]; then 
		jpegtopnm $1 > .tmp.pnm 
	elif [[ $1 == *'.png' ]]; then  
		pngtopnm $1 > .tmp.pnm 
	else
		return
	fi
	head -n 3 .tmp.pnm > header.txt 
	tail -n +4 .tmp.pnm > body.bin
	openssl enc  -des-ecb -nosalt -pass pass:"ANNA" -in body.bin -out body.ecb.bin
	cat header.txt body.ecb.bin > result.ppm 
	rm -f header.txt body.bin body.ecb.bin .tmp.pnm
}


##############################
####    EXTERNAL FILES    ####
##############################

source ~/.bash_profile
source /etc/bash_completion
